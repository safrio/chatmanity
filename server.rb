# app.rb
require 'rubygems'
require 'bundler'
Bundler.setup

require 'faye/websocket'

class Backend
  def notify(event_name, data)

  end
end

App = lambda do |env|
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env)
    backend = Backend.new

    ws.on :message do |event|
      backend.notify(:message, event.data)
      ws.send(event.data)
    end

    ws.on :close do |event|
      p [:close, event.code, event.reason]
      ws = nil
    end

    # Return async Rack response
    ws.rack_response
  end
end
