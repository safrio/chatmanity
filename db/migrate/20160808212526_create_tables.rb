class CreateTables < ActiveRecord::Migration[5.0]
  def change
    enable_extension "uuid-ossp"

    create_table :accounts, id: :uuid do |t|
      t.string :name, null: false
      t.datetime :created_at
    end

    add_index :accounts, :created_at

    create_table :timezones, id: :uuid do |t|
      t.string :name, null: false
      t.integer :offset, null: false
      t.boolean :negative, false
    end

    create_table :users, id: :uuid do |t|
      t.string :email, null: false
      t.integer :status, default: 0
      t.datetime :created_at
    end

    add_index :users, :status
    add_index :users, :created_at

    create_table :memberships, id: :uuid do |t|
      t.uuid :user_id, null: false
      t.uuid :account_id, null: false
      t.integer :role, default: 0
      t.datetime :created_at
    end

    add_index :memberships, [:user_id, :account_id]
    add_index :memberships, [:user_id, :account_id, :role, :created_at], name: :idx_user_account_role_date
    add_index :memberships, :role
    add_index :memberships, :created_at

    create_table :channel_members, id: :uuid do |t|
      t.uuid :channel_id, null: false
      t.uuid :member_id, null: false
      t.uuid :creator_id, null: false
    end

    add_index :channel_members, [:channel_id, :member_id]
    add_index :channel_members, [:channel_id, :member_id, :creator_id], name: :idx_channel_member_creator
    add_index :channel_members, :creator_id

    create_table :channels, id: :uuid do |t|
      t.string :name, default: nil
      t.string :description, default: nil
      t.boolean :general, default: false
    end

    add_index :channels, :general

    create_table :user_profiles, id: :uuid do |t|
      t.string :firstname, null: false
      t.string :lastname
      t.uuid :timezone_id, null: false
    end

    add_index :user_profiles, :timezone_id

    create_table :messages, id: :uuid do |t|
      t.uuid :channel_id, null: false
      t.text :message, null: false
      t.integer :status, default: 0
      t.uuid :sender_id

      t.timestamps
    end

    add_index :messages, :channel_id
    add_index :messages, :status
    add_index :messages, :sender_id

    create_table :message_files, id: :uuid do |t|
      t.uuid :message_id, null: false, index: true
    end

    create_table :message_snippets, id: :uuid do |t|
      t.uuid :message_id, null: false, index: true
    end

    create_table :message_mentions do |t|
      t.uuid :message_id, null: false, index: true
      t.uuid :sender_id, null: false, index: true
      t.uuid :recipient_id, null: false, index: true
    end
  end
end
