class AddUserIdToUserProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :user_id, :uuid, null: false
    add_index :user_profiles, :user_id
  end
end
