# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160809201334) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "accounts", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.index ["created_at"], name: "index_accounts_on_created_at", using: :btree
  end

  create_table "channel_members", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "channel_id", null: false
    t.uuid "member_id",  null: false
    t.uuid "creator_id", null: false
    t.index ["channel_id", "member_id", "creator_id"], name: "idx_channel_member_creator", using: :btree
    t.index ["channel_id", "member_id"], name: "index_channel_members_on_channel_id_and_member_id", using: :btree
    t.index ["creator_id"], name: "index_channel_members_on_creator_id", using: :btree
  end

  create_table "channels", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string  "name"
    t.string  "description"
    t.boolean "general",     default: false
    t.index ["general"], name: "index_channels_on_general", using: :btree
  end

  create_table "memberships", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "user_id",                null: false
    t.uuid     "account_id",             null: false
    t.integer  "role",       default: 0
    t.datetime "created_at"
    t.index ["created_at"], name: "index_memberships_on_created_at", using: :btree
    t.index ["role"], name: "index_memberships_on_role", using: :btree
    t.index ["user_id", "account_id", "role", "created_at"], name: "idx_user_account_role_date", using: :btree
    t.index ["user_id", "account_id"], name: "index_memberships_on_user_id_and_account_id", using: :btree
  end

  create_table "message_files", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "message_id", null: false
    t.index ["message_id"], name: "index_message_files_on_message_id", using: :btree
  end

  create_table "message_mentions", force: :cascade do |t|
    t.uuid "message_id",   null: false
    t.uuid "sender_id",    null: false
    t.uuid "recipient_id", null: false
    t.index ["message_id"], name: "index_message_mentions_on_message_id", using: :btree
    t.index ["recipient_id"], name: "index_message_mentions_on_recipient_id", using: :btree
    t.index ["sender_id"], name: "index_message_mentions_on_sender_id", using: :btree
  end

  create_table "message_snippets", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid "message_id", null: false
    t.index ["message_id"], name: "index_message_snippets_on_message_id", using: :btree
  end

  create_table "messages", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "channel_id",             null: false
    t.text     "message",                null: false
    t.integer  "status",     default: 0
    t.uuid     "sender_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["channel_id"], name: "index_messages_on_channel_id", using: :btree
    t.index ["sender_id"], name: "index_messages_on_sender_id", using: :btree
    t.index ["status"], name: "index_messages_on_status", using: :btree
  end

  create_table "timezones", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string  "name",     null: false
    t.integer "offset",   null: false
    t.boolean "negative"
    t.boolean "false"
  end

  create_table "user_profiles", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string "firstname",   null: false
    t.string "lastname"
    t.uuid   "timezone_id", null: false
    t.uuid   "user_id",     null: false
    t.index ["timezone_id"], name: "index_user_profiles_on_timezone_id", using: :btree
    t.index ["user_id"], name: "index_user_profiles_on_user_id", using: :btree
  end

  create_table "users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "email",                                       null: false
    t.integer  "status",                          default: 0
    t.datetime "created_at"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "updated_at"
    t.string   "remember_me_token"
    t.datetime "remember_me_token_expires_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.index ["created_at"], name: "index_users_on_created_at", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["remember_me_token"], name: "index_users_on_remember_me_token", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", using: :btree
    t.index ["status"], name: "index_users_on_status", using: :btree
  end

end
