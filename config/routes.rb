Rails.application.routes.draw do
  namespace :user do
    resource :signup, only: %w(new create)
    resource :signin, only: %w(new create destroy)
    resource :reset_passwords, only: %w(new create edit update)
  end

  namespace :admin do

  end

  root 'home#index'
end
