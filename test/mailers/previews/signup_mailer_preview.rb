# Preview all emails at http://localhost:3000/rails/mailers/signup_mailer
class SignupMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/signup_mailer/reset_password_notification
  def reset_password_notification
    SignupMailer.reset_password_notification
  end

end
