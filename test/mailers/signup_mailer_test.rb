require 'test_helper'

class SignupMailerTest < ActionMailer::TestCase
  test "reset_password_notification" do
    mail = SignupMailer.reset_password_notification
    assert_equal "Reset password notification", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
