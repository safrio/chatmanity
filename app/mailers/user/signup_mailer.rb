class User::SignupMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.signup_mailer.reset_password_notification.subject
  #
  def reset_password_notification(user)
    @user = user
    @url = edit_user_reset_passwords_url(@user.reset_password_token)

    mail to: user.email, subject: "Your password has been reset"
  end
end
