class MessageService
  def send_message(sender, channel, message)
    channel.messages.create!(
      message: message,
      sender: sender.id
    )

    channel_members_ids(channel)
  end

  private

  def channel_members_ids(channel)
    channel.members.pluck(:id)
  end
end
