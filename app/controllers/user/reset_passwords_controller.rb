class User::ResetPasswordsController < ActionController::Base
  def new
    @user = User.new
  end

  def create
    notice = nil

    if user = User.where(email: params[:user][:email]).first
      notice = if user.reset_password_attempt_expired?
        'Please try yout attempt later.'
      else
        user.deliver_reset_password_instructions!
        'Instructions have been sent to your email.'
      end
    end

    redirect_to root_path, notice: notice
  end

  def edit

  end

  def update

  end
end
