class User::SigninsController < ActionController::Base
  def new
    @user = User.new
  end

  def create
    @user = login(user_params[:email], user_params[:password])

    if @user
      @user.remember_me!
      flash.now[:notice] = 'Login successful'
      redirect_to root_path
    else
      flash.now[:alert] = 'Login failed'
      render action: 'new'
    end
  end

  def destroy
    logout
    flash.now[:notice] = 'Logged out!'
    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end


