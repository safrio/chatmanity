# == Schema Information
#
# Table name: users
#
#  id         :uuid             not null, primary key
#  email      :string           not null
#  status     :integer          default(0)
#  created_at :datetime
#

class User < ApplicationRecord
  authenticates_with_sorcery!

  has_one :profile, class_name: 'User::Profile'

  has_many :memberships
  has_many :accounts, through: :memberships

  has_many :channel_members
  has_many :channels, through: :channel_members

  validates :email, presence: true, uniqueness: true
  validates :password, length: { minimum: 8 }
  validates :password, confirmation: true

  def to_s
    if profile.present?
      profile.to_s
    else
      email
    end
  end
end
