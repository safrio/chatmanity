# == Schema Information
#
# Table name: user_profiles
#
#  id          :uuid             not null, primary key
#  firstname   :string           not null
#  lastname    :string
#  timezone_id :uuid             not null
#

class User::Profile < ApplicationRecord
  belongs_to :user, required: true
  belongs_to :timezone, required: true

  validates :firstname, presence: true

  def to_s
    [firstname, lastname].join(' ')
  end
end
