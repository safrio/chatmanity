# == Schema Information
#
# Table name: channel_members
#
#  id         :uuid             not null, primary key
#  channel_id :uuid             not null
#  member_id  :uuid             not null
#  creator_id :uuid             not null
#

class Channel::Member < ApplicationRecord
  belongs_to :channel
  belongs_to :member, class_name: 'User'
  belongs_to :creator, class_name: 'User'

  validates :channel_id, :member_id, :creator_id, presence: true
end
