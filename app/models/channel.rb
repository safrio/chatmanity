# == Schema Information
#
# Table name: channels
#
#  id          :uuid             not null, primary key
#  name        :string
#  description :string
#  general     :boolean          default(FALSE)
#

class Channel < ApplicationRecord
  has_many :channel_members, class_name: 'Channel::Member'
  has_many :members, through: :channel_members
  has_many :messages
end
