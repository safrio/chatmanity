# == Schema Information
#
# Table name: messages
#
#  id         :uuid             not null, primary key
#  channel_id :uuid             not null
#  message    :text             not null
#  status     :integer          default(0)
#  sender_id  :uuid
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Message < ApplicationRecord
  belongs_to :channel, required: true
  belongs_to :sender, class_name: 'User', required: true

  has_many :files, class_name: 'Message::File'
  has_many :mentions, class_name: 'Message::Mention'
  has_many :snippets, class_name: 'Message::Snippet'

  validates :message, presence: true
end
