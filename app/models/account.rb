# == Schema Information
#
# Table name: accounts
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  created_at :datetime
#

class Account < ApplicationRecord
  has_many :memberships
  has_many :users, through: :memberships

  validates :name, presence: true
end
