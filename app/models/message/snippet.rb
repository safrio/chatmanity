# == Schema Information
#
# Table name: message_snippets
#
#  id         :uuid             not null, primary key
#  message_id :uuid             not null
#

class Message::Snippet < ApplicationRecord
  belongs_to :message, required: true
end
