# == Schema Information
#
# Table name: message_files
#
#  id         :uuid             not null, primary key
#  message_id :uuid             not null
#

class Message::File < ApplicationRecord
  belongs_to :message, required: true
end
