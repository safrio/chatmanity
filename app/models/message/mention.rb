# == Schema Information
#
# Table name: message_mentions
#
#  id           :integer          not null, primary key
#  message_id   :uuid             not null
#  sender_id    :uuid             not null
#  recipient_id :uuid             not null
#

class Message::Mention < ApplicationRecord
  belongs_to :message, required: true
end
