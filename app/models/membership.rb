# == Schema Information
#
# Table name: memberships
#
#  id         :uuid             not null, primary key
#  user_id    :uuid             not null
#  account_id :uuid             not null
#  role       :integer          default(0)
#  created_at :datetime
#

class Membership < ApplicationRecord
  belongs_to :user, required: true
  belongs_to :account, required: true
end
