# == Schema Information
#
# Table name: timezones
#
#  id       :uuid             not null, primary key
#  name     :string           not null
#  offset   :integer          not null
#  negative :boolean
#  false    :boolean
#

class Timezone < ApplicationRecord
  validates :name, :offset, presence: true
end
